import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import MasterPage from '../components/MasterPage.vue'
import Token from "@/token-usage.js";

const routes = [
  {
    path: '/home',
    component: MasterPage,
    meta: {
      forLoggedIn: true,
    },
    children: [
      {
        path: '',
        name: 'home',
        component: HomeView
      },
      {
        path: '/',
        name: 'about',
        meta: {
          forLoggedIn: false,
        },
        component: ()=> import("../views/AboutView.vue")
      },
      {
        path: '/add',
        name: 'addView',
        component: ()=> import("../views/AddToGalaryView.vue")
      },
      {
        path: '/profile',
        name: 'profile',
        meta: {
          forLoggedIn: false,
        },
        component: ()=> import("../views/ProfileView.vue")
      },
      
    ]
  },
  {
    path: '/:catchAll(.*)',
    name: 'notFound',
    component: () => import("../views/NotFoundView.vue") 
  },
  {
    path: "/login",
    name: "login",
    meta: {
      forLoggedIn: false,
    },
    component: ()=> import("../views/LoginView.vue"),
  },
  {
    path: "/dialog",
    name: "dialog",
    meta: {
      forLoggedIn: false,
    },
    component: ()=> import("../views/DialogView.vue"),
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const accessToken = Token.getAccessTokenFromCookie();
  const nextRouteIsForLoggedIn = to.meta?.forLoggedIn;

  if (nextRouteIsForLoggedIn && !accessToken) {
    next("/dialog");
  } else if (to.name === "login" && accessToken) {
    next("/");
  } else {
    next();
  }
});

export default router;
